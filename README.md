# FreshRSS - "Explosm daily comic" extension

This FreshRSS extension allows you to directly enjoy the [daily Explosm comic](https://explosm.net/) within your FreshRSS installation.

To use it, upload the ```xExtension-Explosm``` directory to the FreshRSS `./extensions` directory on your server and enable it on the extension panel in FreshRSS.

## Acknowledgments

This extension is highly inspired by the one created by [Kevin Papst](https://github.com/kevinpapst) for the [Dilbert](https://github.com/kevinpapst/freshrss-dilbert) feed. All credit goes to him!

## The Explosm feed itself

This extension supports the following feed address: https://explosm.net/rss.xml.

But this extension will ONLY work on new added feed items as it manipulates them while fetching new items.

## Fair use

Please note that the images are not embedded in the Explosm RSS feed, in order to get more visitors on the website. So please play fair and visit them on a daily base instead of using this extension.

## Requirements

This FreshRSS extension uses the PHP extension [DOM](http://php.net/dom) and [XML](http://php.net/xml).

As those are requirements by [FreshRSS](https://github.com/FreshRSS/FreshRSS) itself, you should be good to go.

## Installation

The first step is to put the extension into your FreshRSS extension directory:
```
cd /var/www/FreshRSS/extensions/
wget https://framagit.org/dohseven/freshrss-explosm/-/archive/master/freshrss-explosm-master.zip
unzip freshrss-explosm-master.zip
mv freshrss-explosm-master/xExtension-Explosm .
rm -rf freshrss-explosm-master/ freshrss-explosm-master.zip
```

Then switch to your browser https://localhost/FreshRSS/p/i/?c=extension and activate it.

## About FreshRSS
[FreshRSS](https://freshrss.org/) is a great self-hosted RSS Reader written in PHP, which is can also be found here at [GitHub](https://github.com/FreshRSS/FreshRSS).

More extensions can be found at [FreshRSS/Extensions](https://github.com/FreshRSS/Extensions).

## Changelog

* 0.1.1: Fix for the URL scheme dropping 'www.', see #1.
* 0.1.0: First release.
